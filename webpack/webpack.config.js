const path = require('path');

module.exports = {
  context: path.resolve(__dirname, 'source'),
  entry: {
    "scripts": "./js/only-js/main"
  },
  resolveLoader: {},
  resolve: {
    extensions: ['.js'],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
};
