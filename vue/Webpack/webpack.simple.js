const path = require('path');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    "scripts": "./js/only-js"
  },
  resolveLoader: {},
  resolve: {
    extensions: ['.js'],
  },
  output: {
    filename: '[name].js',
    publicPath: 'http://localhost:8080/',
    path: path.resolve(__dirname, 'dist')
  },
};
