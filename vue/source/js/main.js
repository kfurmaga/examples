import PointPicker from './components/PointPicker';
import StringList from './components/StringList';
import VModelExample from './components/VModelExample';
import HandlersExample from './components/HandlersExample';
import StoreExample from './components/StoreExample';
import GettersExample from './components/GettersExample';
import KarasExample from './components/KarasExample';
import KarasList from './components/KarasList';

import init from "./init";

init([PointPicker, StringList, VModelExample, HandlersExample, StoreExample, GettersExample, KarasExample, KarasList]);
