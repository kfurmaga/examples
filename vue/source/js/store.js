import Vue from "vue";
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 11
  },
  getters: {
    missisipiCount: function (state) {
      return state.count + " missisipi";
    }
  },
  actions: {
    increment: function ({commit}) {
      commit('incrementCount')
    },
    incrementAndNotify: function ({dispatch}) {
      return dispatch('increment').then(() => {
        console.log('triggered increment action');
      });
    }
  },
  mutations: {
    incrementCount: function (state) {
      state.count++;
    }
  }
});
