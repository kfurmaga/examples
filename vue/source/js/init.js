import Vue from 'vue';
import store from './store';

export default function init(components) {
  components.forEach(component => {
    Vue.component(component.name, component)
  });

  const vue = new Vue({
    el: "#vue",
    store,
    data: function () {
      return {
        model: window.model || {}
      };
    }
  });
}