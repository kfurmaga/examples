const path = require('path');

const options = {
  rootDir: path.resolve(__dirname),
  developmentHostPath: 'http://localhost:8080/',
  sourceDir: path.resolve(__dirname, 'source'),
  outputDir: path.resolve(__dirname, 'dist'),
};

switch (process.env.NODE_ENV) {
  case 'prod':
  case 'production':
    options.env = 'production';
    break;
  case 'dev':
  case 'development':
  default:
    options.env = 'development';
    break;
}

const configPath = `./Webpack/webpack.${options.env}.js`;
const createConfig = require(configPath);

module.exports = createConfig(options);
