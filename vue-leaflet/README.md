# Vue example project

to initialize project execute:
```
npm install
```

to build project execute:
```
npm run build
```

to start development environment execute:
```
npm run serve
```