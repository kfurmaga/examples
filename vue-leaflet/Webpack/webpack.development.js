﻿const path = require('path');
const AssetsWebpackPlugin = require('assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const createBaseConfig = require('./webpack.base.js');

function createDevelopmentConfig(options) {
  return {
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [{
            loader: "style-loader" // creates style nodes from JS strings
          }, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          }]
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            include: [
              path.resolve(__dirname, 'node_modules'),
              path.resolve(__dirname, 'node_modules/leaflet/dist/images'),
            ],
            loaders: {
              js: {
                loader: 'babel-loader',
                options: {
                  presets: ['babel-preset-env'],
                  plugins: ['transform-object-rest-spread']
                }
              }
            }
          }
        },
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['babel-preset-env'],
              plugins: ['transform-object-rest-spread']
            }
          }
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader'
        },
      ]
    },
    devtool: 'inline-source-map',
    devServer: {},
    plugins: [
      new AssetsWebpackPlugin({
        filename: 'assets.json',
        path: options.rootDir,
        prettyPrint: true
      }),
      new HtmlWebpackPlugin({
        'template': 'index.html'
      })
    ],
    output: {
      filename: '[name].js',
      publicPath: options.developmentHostPath,
      path: options.outputDir
    },
  };
}

module.exports = function (options) {
  return Object.assign(createBaseConfig(options), createDevelopmentConfig(options));
};
