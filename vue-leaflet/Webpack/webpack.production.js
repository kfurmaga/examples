﻿const path = require('path');
const AssetsWebpackPlugin = require('assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsWebpackPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const createBaseConfig = require('./webpack.base.js');

const extractStylesPlugin = new ExtractTextWebpackPlugin({filename: '[name].[chunkhash].css', allChunks: true});

function createProductionConfig(options) {
  return {
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: extractStylesPlugin.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  minimize: true,
                  importLoaders: 1
                }
              },
              {
                loader: 'sass-loader'
              }
            ]
          })
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            include: [
              path.resolve(__dirname, 'node_modules'),
              path.resolve(__dirname, 'node_modules/leaflet/dist/images'),
            ],
            loaders: [
              'scss-loader',
              'css-loader',
              'style-loader'
            ]
          }
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader'
        },
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: '"production"'
        }
      }),
      new AssetsWebpackPlugin({
        filename: 'assets.json',
        path: options.rootDir
      }),
      new UglifyJsWebpackPlugin(),
      extractStylesPlugin
    ],
    stats: {
      hash: true,
      version: true,
      timings: true,
      assets: false,
      chunks: false,
      modules: false,
      reasons: true,
      children: false,
      source: false,
      errors: true,
      errorDetails: true,
      warnings: true,
      publicPath: true
    },
    output: {
      filename: '[name].[chunkhash].js',
      path: options.outputDir
    }
  };
}

module.exports = function (options) {
  return Object.assign(createBaseConfig(options), createProductionConfig(options));
};
