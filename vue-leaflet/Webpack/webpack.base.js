﻿const path = require('path');
const entries = require('./entries');

module.exports = function (options) {
  return {
    context: options.sourceDir,
    entry: entries(),
    resolveLoader: {
      alias: {
        'scss-loader': 'sass-loader'
      },
      modules: [
        'node_modules'
      ]
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
      },
      modules: [
        'node_modules'
      ]
    },
  };
};
