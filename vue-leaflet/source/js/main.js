import Leaflet from './leaflet/Component'
import LeafletExample from './leaflet/Example'
import init from "./init";

init([Leaflet, LeafletExample]);
