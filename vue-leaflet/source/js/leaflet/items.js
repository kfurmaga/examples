import 'leaflet';
import './L.ImageTransform';

export class LeafletItem {
  get mapItem() {
    return this._mapItem;
  }
}
export class Image extends LeafletItem {
  constructor(url, topLeft, topRight, bottomLeft) {
    super();

    const bottomRight = new L.LatLng(
      topRight.lat + (bottomLeft.lat - topLeft.lat),
      topRight.lng + (bottomLeft.lng - topLeft.lng)
    );

    const anchors = [topLeft, topRight, bottomRight, bottomLeft];
    this._mapItem = L.imageTransform(url, anchors)
  }

  get mapItem() {
    return this._mapItem
  }
}

export class Marker extends LeafletItem {
  constructor(position) {
    super();
    const icon = Marker.createIcon();
    this._mapItem = new L.marker(position, {icon: icon});
  }

  get position() {
    return this._mapItem.getLatLng();
  }

  set position(value) {
    this._mapItem.setLatLng(value);
  }

  static createIcon() {
    return L.icon({
      iconUrl: 'http://leafletjs.com/examples/custom-icons/leaf-green.png',
      shadowUrl: 'http://leafletjs.com/examples/custom-icons/leaf-shadow.png',

      iconSize:     [38, 95], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62],  // the same for the shadow
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
  }
}

export class Zone extends LeafletItem {
  constructor(position, radius) {
    super();

    const options = {
      color: '#337ab7',
      fillColor: '#3da8ff',
      fillOpacity: 0.8
    };

    this._mapItem = L.circle(position, radius, options);
  }
}