import Vue from 'vue';
export default function init(components) {
  components.forEach(component => {
    Vue.component(component.name, component)
  });

  const vue = new Vue({
    el: "#vue",
    data: function () {
      return {
        model: window.model || {}
      };
    }
  });
}